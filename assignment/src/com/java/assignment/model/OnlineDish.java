package com.java.assignment.model;

public class OnlineDish extends Dish {
	private String type;
	private boolean gfd;
	private boolean vgd;
	private boolean hmd;
	private boolean sfd;
	
	public OnlineDish(String name, String tp, boolean gd,  boolean vd,  boolean hd,  boolean sd) {
		dishName = name;
		type = tp;
		gfd = gd;
		vgd = vd;
		hmd = hd;
		sfd = sd;
	}
	
	@Override
	public String toString() { 
		return "type: " + type + ", gluten free: " + gfd + ", veggie: " + vgd + ", halal: " + hmd + ", sfd: " + sfd;
	}

	public boolean getGfd() {
		return gfd;
	}
	public void setGfd(boolean gfd) {
		this.gfd = gfd;
	}
	public boolean getVgd() {
		return vgd;
	}
	public void setVgd(boolean vgd) {
		this.vgd = vgd;
	}
	public boolean getHmd() {
		return hmd;
	}
	public void setHmd(boolean hmd) {
		this.hmd = hmd;
	}
	public boolean getSfd() {
		return sfd;
	}
	public void setSfd(boolean sfd) {
		this.sfd = sfd;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
