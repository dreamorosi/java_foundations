package com.java.assignment.model;

public class Order {
	private String customerName;
	private String extras;
		
	public Order(String name, String extr) {
		customerName = name;
		extras = extr;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public String getExtras() {
		return extras;
	}

	public void setExtras(String extras) {
		this.extras = extras;
	}

	@Override
	public String toString() {
		return "CustomerName: " + customerName + ", extras: " + extras;
	}
}
