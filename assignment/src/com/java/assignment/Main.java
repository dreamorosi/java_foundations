package com.java.assignment;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

import com.java.assignment.model.OnlineDish;
import com.java.assignment.model.Order;

public class Main {

	static List<Order> orders;
	static List<OnlineDish> dishes;
	
	public static void main(String[] args) {
		orders = new ArrayList<Order>();
		dishes = new ArrayList<OnlineDish>();
		
		try {
			File file = new File("../online-order-sample.csv");
			Scanner scanner = new Scanner(file);
			scanner.useDelimiter(System.getProperty("line.separator"));
			scanner.next();
			while (scanner.hasNext()) {
				String line = scanner.next();
				String[] strArray = line.split(",");

				OnlineDish dish = new OnlineDish(strArray[1], strArray[2], Boolean.valueOf(strArray[3]), 
						Boolean.valueOf(strArray[4]), Boolean.valueOf(strArray[5]), Boolean.valueOf(strArray[6]));
				dishes.add(dish);
				
				Order order = new Order(strArray[0], strArray[7]);
				orders.add(order);
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		OnlineStore salesManager = new OnlineStore();
		
		System.out.println("Total orders: " + salesManager.getNumberOrders(orders));
		System.out.println("\n");
		System.out.println("Order No." + 1 + ": " + salesManager.getOrder(orders, 1).toString());
		System.out.println("\n");
		System.out.println("Order List: " + salesManager.getAllOrdersToString(orders));
		System.out.println("\n");
		System.out.println("Dish No." + 2 + ": " + salesManager.getDish(dishes, 2));
		System.out.println("\n");
		System.out.println("Dish List: " + salesManager.getAllDishToString(dishes));
		System.out.println("\n");
		System.out.println("Dishes of Type mc" + salesManager.getDishesByType(dishes, "mc"));
		System.out.println("\n");
		System.out.println("Dishes By Features: " + salesManager.getDishesByFeature(dishes, "sfd"));
		System.out.println("\n");
		System.out.println("Stats by dish type: " + salesManager.getStatsByDishType(dishes, "st") + "% of dishes are type st");
	}

}
