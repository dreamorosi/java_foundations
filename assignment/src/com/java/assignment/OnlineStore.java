package com.java.assignment;

import java.util.ArrayList;
import java.util.List;

import com.java.assignment.model.OnlineDish;
import com.java.assignment.model.Order;

public class OnlineStore implements OnlineOrderOps<Order, OnlineDish> {
	@Override
	public int getNumberOrders(List<Order> orderList) {
		return orderList.size();
	}

	@Override
	public Order getOrder(List<Order> orderList, int orderIndex) {
		return orderList.get(orderIndex);
	}

	@Override
	public OnlineDish getDish(List<OnlineDish> dishList, int dishIndex) {
		return dishList.get(dishIndex);
	}
	
	@Override
	public String getAllOrdersToString(List<Order> orderList) {
		String str = "\n";
		for (Order order : orderList) {
			str += order.toString() + "\n";
		}
		return str;
	}

	@Override
	public String getAllDishToString(List<OnlineDish> dishList) {
		String str = "\n";
		for (OnlineDish order : dishList) {
			str += order.toString() + "\n";
		}
		return str;
	}

	@Override
	public List<OnlineDish> getDishesByType(List<OnlineDish> dishList, String dishType) {
		List<OnlineDish> result = new ArrayList<OnlineDish>();
		for (OnlineDish dish : dishList) {
			if (dish.getType().equals(dishType)) {
				result.add(dish);
			}
		}
		return result;
	}

	@Override
	public List<OnlineDish> getDishesByFeature(List<OnlineDish> dishList, String feature) {
		List<OnlineDish> result = new ArrayList<OnlineDish>();
		for (OnlineDish dish : dishList) {
			if (feature == "gfd") {
				if (dish.getGfd()) {
					result.add(dish);
				}
			} else if (feature == "vgd") {
				if (dish.getVgd()) {
					result.add(dish);
				}
			} else if (feature == "hmd") {
				if (dish.getHmd()) {
					result.add(dish);
				}

			} else if (feature == "sfd") {
				if (dish.getSfd()) {
					result.add(dish);
				}
			}
		}
		return result;
	}

	@Override
	public String getStatsByDishType(List<OnlineDish> dishList, String dishType) {
		int allByType = 0;
		for (OnlineDish dish : dishList) {
			if (dish.getType().equals(dishType))
				allByType++;
		}
		int totalDishes = dishList.size();
		float percentageType = (allByType * 100) / totalDishes;
		return String.valueOf(percentageType);
	}
}
